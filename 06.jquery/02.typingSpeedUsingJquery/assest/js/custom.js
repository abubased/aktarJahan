$(document).ready(function(){
    let startTime;
    //সাবমিট বাটনে ক্লিক করবো ।
    $("#btn1").click(function(){
        //অবশ্যই userName এ ভেলু থা্কতে হবে 
        if($("#userName").val().length>1){
            //userName ইনপুট থেকে তার ভেলু স্টোর করবো ।
            let name = $("#userName").val();
            // স্টোরকরা ভেলুটি user এ বসাবো 
            $("#user").text(name);
            $("#getUserName").hide();
            $("#greeting,#btn").show();
        }
    });
    //btn এ ক্লিক করলে ।
    $("#btn").click(function(){
        //Texarea show করাবো ।
        $("#userString,#message").show();
         //যদি বাটনের ভিতরের text start হয় 
        if($("#btn").text()=="Start"){
          //start বাটনে ক্লিক করলে তার লেখাটি চেইঞ্জ হয়ে Done করবো ।
            $("#btn").text("Done");
            //userString অটোমেটিক ফোকাস করবো ।
            $("#userString").focus();
            //message এ এ্কটি রে্নডম বাক্য দেখাবো ।
            $.ajax({
                url:"assest/file/keywords.json",
                dataType:"json",
                success:function(data){
                    let totalLine = data.length;
                    let randomNumber = Math.floor(Math.random()*totalLine);
                    $("#message").text(data[randomNumber]);
                }
            });
            //টাইপিং শুরুর সময় স্টোর করবো ।  
            startTime = new Date($.now());
        }
        //যদি বাটনের ভি্তরের text Start না হয় 
        else{
            //userString থেকে টাইপকরা ভেলু স্টোর করবো 
                let userText = $("#userString").val();
            //userString থেকে ভেলু এবং ফোকাস আউট করবো ।
                $("#userString").val(null).blur();
            let randomMessage = $("#message").text();
            let correctWords = compareWords(randomMessage,userText);
            
            if(correctWords>1){
                $("#userString").hide();
                //বাটনে এ ক্লিক করলে তার লেখাটি Start এ পরিবর্তন করবো ।
                $("#btn").text("Start");
                //টাইপিং শেষের সময় স্টোর করবো ।
                let endTime = new Date($.now());
            //সর্বমোট সময় বের করবো ।
                let spendTime = Math.round((endTime-startTime)/1000);
                //message এ রেজাল্ট দেখাবো ।
                let totalWords = userText.split(" ").length;
                let speed = Math.round((totalWords/spendTime)*60);
                let reasult = showReasult(speed,correctWords);
                $("#message").text(reasult);
                $("#message").css("margin-bottom","20px"); 
            }              
        }
        
    }); 
    function compareWords(x,y){
         let wordsOne = x.split(" ");
         let wordsTwo = y.split(" ");
         let correctWords = 0;
         wordsOne.forEach(function(item,index){
             if(item==wordsTwo[index]){
                 correctWords++;
             }
         });
         return correctWords;
     };
    function showReasult(x,y){
        var totalWords = $("#message").text().split(" ");
        return "You can type "+x+" words per minute. and you are type "+y+" correct words & "+(totalWords.length-y)+" wrong words.";
    };
    
});