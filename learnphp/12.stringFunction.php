<?php define('title','String Function') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>
    <?php 
    
    $message = "Queue a new e-mail message for sending after (n) seconds. (Laravel Snippets) Mail later";

    echo strlen($message)."<br>";

    echo strlen("Queue a new e-mail message for sending after (n) seconds. (Laravel Snippets) Mail later");

    echo "<br>".str_word_count($message);
    
    echo nl2br("\n".str_replace("Snippets","PHP",$message,$n)."\n".$n);


    ?>
</body>
</html>

