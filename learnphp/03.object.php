<?php define('title','Object in Php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <?php 
       echo '<h1>'.title.'</h1><hr>';

    class room{
       public $tvbrand = 'samsung';
       private $tvstatus = 'OFF';

       public function changeTvStatus()
       {
            $this->tvstatus = 'ON';
            return  $this->tvstatus;
       }
    }

    $room = new room;
    echo $room->tvbrand.'<br>';
    $status = $room->changeTvStatus();
    echo $status;
    ?>
</body>
</html>

