<?php define('title','Array Function') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>

    <?php 
        $faraz = [
            'fazar',
            'jahar',
            'asar',
            'magrib',
            'asa'
         ];

        $sunnat = [
            'fazar',
            'jahar',
            'magrib',
            'asa'
        ];
        echo "<h2>সকল ফরয নামাজের ওয়াক্ত সুমহঃ</h2><hr><pre>";
        
        print_r($faraz);

        echo "</pre><h2>সকল সুন্নাত নামাজের ওয়াক্ত সুমহঃ</h2><hr><pre>";

        print_r($sunnat);

        $salat = array_merge($faraz,$sunnat);

        echo "</pre><h2>সকল ফরয এবং সুন্নাত নামাজের ওয়াক্ত সুমহঃ</h2><hr><pre>";

        print_r($salat);

        echo "</pre><h2>মোট  নামাজের ওয়াক্তঃ</h2><hr><pre>";

        echo count($salat)."<br>";

        echo "</pre><h2>ফরয এবং সুন্নাত নামাজঃ</h2><hr><pre>";

        print_r(array_count_values($salat));

        echo "</pre><h2>ফরয নামাজে আসর আছে কিনাঃ </h2><hr><pre>";
        $value = (!in_array('asar',$faraz)) ? 'NO ' : 'YES!'; 

        echo $value."<br>";
        echo "</pre><h2>সুন্নাত নামাজে আসর আছে কিনাঃ</h2><hr><pre>";
        if(!in_array('asar', $sunnat)){
            echo "NO";
        }else{
            echo "YES!";
        }

        array_push($salat,'beter');

        echo "</pre><h2>সকল ফরয এবং সুন্নাত নামাজের ওয়াক্ত সুমহের সাথে বেতর নামাজ যুক্ত করাঃ</h2><hr><pre>";

        print_r($salat);

    ?>


</body>
</html>

