<?php define('title','If and Else') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>

    <?php
    $x = 25;

    if($x == 5){
        //  if condition is true code wil be go here 
        echo 'Yes! X = 5';
    }else if($x==15){
        //  if condition is false code wil be go here 
        echo 'Yes! X = 15';
    }else{
        //  if condition is false code wil be go here 
        echo 'NO! X is not equal 5 or 15';
    }

    echo "<h1>Swith statement</h1><hr>";
    
    $y = 3+2;
    switch ($y) {
        case 5:
            echo 'X = 5';
            break;
        case 15:
            echo 'X = 15';
            break;
        case 25:
            echo 'X = 25';
            break;
        default:
            echo 'NO! X is not equal 5 or 15 or 25';
            break;
    }

    ?>
</body>
</html>

