<?php define('title','For Each Loop') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>
    <?php 


    $myArray=[
        'html'=>'Hyper text markup language',
        'css' => 'Cascading Stylesheet',
        'php' => 'preprocessor hypertext'
    ];
 
    foreach ($myArray as $key => $value) {
        echo $key." = ".$value."<br>";
    }      
    ?>
</body>
</html>

