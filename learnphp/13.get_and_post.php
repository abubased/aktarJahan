<?php define('title','Get and Post') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>
    <h2>GET:</h2>

    <form method="get" action="13.get_and_post.php">
        <input type="text" name="search" placeholder="Type Keywords">
        <br>
        <input type="text" name="brand" placeholder="Type Your Brand">
        <button type="submit">Submit</button>
    </form>

    <h2>POST:</h2>

    <form method="post" action="13.get_and_post.php">
        <input type="text" name="name" placeholder="Full Name">
        <br>
        <input type="email" name="mail" placeholder="Type Your Email">
        <button type="submit">Submit</button>
    </form>

    <?php
        if($_GET){

            if(!empty($_GET['search']) && !empty($_GET['brand'])){
                $data['search'] = $_GET['search'];
                $data['brand'] = $_GET['brand'];
                echo "<pre>";
                print_r($data);
            }else{
                echo "please! Fillup the form";
            }
           
        }elseif($_POST){
            if(empty($_POST['name'])){
                echo "Please! Type your Name";
            }elseif(empty($_POST['mail'])){
                echo "Please! Type your Mail This Required";
            }else{
                $data['name'] = $_POST['name'];
                $data['mail'] = $_POST['mail'];
                echo "<pre>";
                print_r($data);
            }
        }
    ?>

</body>
</html>

