<?php define('title','Function') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo title; ?></title>

</head>
<body>
    <h1><?php echo title.':'; ?></h1><hr>
    <?php 
    
    function FunctionName()
    {
        echo "this is a function";
    };

    FunctionName();
    echo "<br>";

    function sum($a,$b){
       return $a+$b;
    }
    echo sum(5,10);
    
    ?>
</body>
</html>

